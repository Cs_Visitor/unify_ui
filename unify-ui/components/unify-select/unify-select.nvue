<template>
	<view class="row-wrap">
		<view class="row-middle-center radius-xs margin-xs padding-lr-md" :class="[item.checked?activeBg:inactiveBg]" :style="{width:width+'rpx',height:height+'rpx'}"
		 @tap="select(index)" v-for="(item,index) in selectData" :key="index">
			<text class="text-center ellipsis-1 flex-1" :style="{fontSize:size+'rpx'}" :class="[item.checked?activeColor:inactiveColor]">{{item.name}}</text>
		</view>
	</view>
</template>

<script>
	/**
	 * select 选择组件
	 * @description 用于单选与多选
	 * @tutorial 
	 * @property {Array} unify-data 数据
	 * @property {String, Number} size 字号
	 * @property {String} mode = [radio|check] 类型
	 * @value radio 单选
	 * @value check 复选
	 * @property {String, Number} height 高度
	 * @property {String, Number} width 宽度，默认为空值，空值状态弹性布局
	 * @property {String} active-bg = [bg-theme|bg-theme-help|bg-red|bg-red-help|bg-orange|bg-orange-help|bg-yellow|bg-yellow-help|bg-green|bg-green-help|bg-cyan|bg-cyan-help|bg-blue|bg-blue-help|bg-purple|bg-purple-help|bg-black|bg-black-help|bg-gray|bg-gray-help|bg-white|bg-white-help|bg-theme-top|bg-theme-bottom|bg-theme-left|bg-theme-right|bg-red-top|bg-red-bottom|bg-red-left|bg-red-right|bg-orange-top|bg-orange-bottom|bg-orange-left|bg-orange-right|bg-yellow-top|bg-yellow-bottom|bg-yellow-left|bg-yellow-right|bg-green-top|bg-green-bottom|bg-green-left|bg-green-right|bg-cyan-top|bg-cyan-bottom|bg-cyan-left|bg-cyan-right|bg-blue-top|bg-blue-bottom|bg-blue-left|bg-blue-right|bg-purple-top|bg-purple-bottom|bg-purple-left|bg-purple-right|bg-black-top|bg-black-bottom|bg-black-left|bg-black-right|bg-gray-top|bg-gray-bottom|bg-gray-left|bg-gray-right|bg-white-top|bg-white-bottom|bg-white-left|bg-white-right] 激活背景色
	 * @value bg-theme 主题背景主色，可在uni.scss自行配置颜色值
	 * @value bg-theme-help 主题背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-theme-top 主题色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-theme-bottom 主题色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-theme-left 主题色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-theme-right 主题色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-red 红色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-red-help 红色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-red-top 红色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-red-bottom 红色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-red-left 红色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-red-right 红色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-orange 橙色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-orange-help 橙色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-orange-top 橙色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-orange-bottom 橙色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-orange-left 橙色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-orange-right 橙色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-yellow 黄色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-yellow-help 黄色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-yellow-top 黄色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-yellow-bottom 黄色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-yellow-left 黄色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-yellow-right 黄色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-green 绿色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-green-help 绿色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-green-top 绿色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-green-bottom 绿色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-green-left 绿色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-green-right 绿色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-cyan 青色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-cyan-help 青色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-cyan-top 青色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-cyan-bottom 青色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-cyan-left 青色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-cyan-right 青色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-blue 蓝色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-blue-help 蓝色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-blue-top 蓝色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-blue-bottom 蓝色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-blue-left 蓝色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-blue-right 蓝色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-purple 紫色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-purple-help 紫色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-purple-top 紫色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-purple-bottom 紫色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-purple-left 紫色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-purple-right 紫色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-black 黑色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-black-help 黑色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-black-top 黑色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-black-bottom 黑色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-black-left 黑色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-black-right 黑色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-gray 灰色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-gray-help 灰色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-gray-top 灰色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-gray-bottom 灰色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-gray-left 灰色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-gray-right 灰色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-white 白色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-white-help 白色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-white-top 白色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-white-bottom 白色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-white-left 白色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-white-right 白色渐变（左主右辅），由主色与辅色渐变而成
	 * @property {String} inactive-bg = [bg-theme|bg-theme-help|bg-red|bg-red-help|bg-orange|bg-orange-help|bg-yellow|bg-yellow-help|bg-green|bg-green-help|bg-cyan|bg-cyan-help|bg-blue|bg-blue-help|bg-purple|bg-purple-help|bg-black|bg-black-help|bg-gray|bg-gray-help|bg-white|bg-white-help|bg-theme-top|bg-theme-bottom|bg-theme-left|bg-theme-right|bg-red-top|bg-red-bottom|bg-red-left|bg-red-right|bg-orange-top|bg-orange-bottom|bg-orange-left|bg-orange-right|bg-yellow-top|bg-yellow-bottom|bg-yellow-left|bg-yellow-right|bg-green-top|bg-green-bottom|bg-green-left|bg-green-right|bg-cyan-top|bg-cyan-bottom|bg-cyan-left|bg-cyan-right|bg-blue-top|bg-blue-bottom|bg-blue-left|bg-blue-right|bg-purple-top|bg-purple-bottom|bg-purple-left|bg-purple-right|bg-black-top|bg-black-bottom|bg-black-left|bg-black-right|bg-gray-top|bg-gray-bottom|bg-gray-left|bg-gray-right|bg-white-top|bg-white-bottom|bg-white-left|bg-white-right] 未激活背景色
	 * @value bg-theme 主题背景主色，可在uni.scss自行配置颜色值
	 * @value bg-theme-help 主题背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-theme-top 主题色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-theme-bottom 主题色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-theme-left 主题色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-theme-right 主题色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-red 红色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-red-help 红色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-red-top 红色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-red-bottom 红色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-red-left 红色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-red-right 红色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-orange 橙色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-orange-help 橙色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-orange-top 橙色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-orange-bottom 橙色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-orange-left 橙色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-orange-right 橙色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-yellow 黄色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-yellow-help 黄色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-yellow-top 黄色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-yellow-bottom 黄色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-yellow-left 黄色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-yellow-right 黄色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-green 绿色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-green-help 绿色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-green-top 绿色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-green-bottom 绿色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-green-left 绿色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-green-right 绿色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-cyan 青色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-cyan-help 青色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-cyan-top 青色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-cyan-bottom 青色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-cyan-left 青色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-cyan-right 青色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-blue 蓝色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-blue-help 蓝色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-blue-top 蓝色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-blue-bottom 蓝色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-blue-left 蓝色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-blue-right 蓝色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-purple 紫色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-purple-help 紫色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-purple-top 紫色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-purple-bottom 紫色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-purple-left 紫色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-purple-right 紫色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-black 黑色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-black-help 黑色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-black-top 黑色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-black-bottom 黑色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-black-left 黑色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-black-right 黑色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-gray 灰色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-gray-help 灰色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-gray-top 灰色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-gray-bottom 灰色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-gray-left 灰色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-gray-right 灰色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-white 白色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-white-help 白色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-white-top 白色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-white-bottom 白色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-white-left 白色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-white-right 白色渐变（左主右辅），由主色与辅色渐变而成
	 * @property {String} active-color = [color-main|color-often|color-help|color-fill|color-theme|color-theme-help|color-red|color-red-help|color-orange|color-orange-help|color-yellow|color-yellow-help|color-green|color-green-help|color-cyan|color-cyan-help|color-blue|color-blue-help|color-purple|color-purple-help|color-black|color-black-help|color-gray|color-gray-help|color-white|color-white-help] 激活文本颜色
	 * @value color-main 主要文字颜色
	 * @value color-often 常规文字颜色
	 * @value color-help 次要/辅助文字颜色
	 * @value color-fill 占位文字颜色
	 * @value color-theme 主题文字主颜色
	 * @value color-theme-help 主题文字辅颜色
	 * @value color-red 红色文字主颜色
	 * @value color-red-help 红色文字辅颜色
	 * @value color-orange 橙色文字主颜色
	 * @value color-orange-help 橙色文字辅颜色
	 * @value color-yellow 黄色文字主颜色
	 * @value color-yellow-help 黄色文字辅颜色
	 * @value color-green 绿色文字主颜色
	 * @value color-green-help 绿色文字辅颜色
	 * @value color-cyan 青色文字主颜色
	 * @value color-cyan-help 青色文字辅颜色
	 * @value color-blue 蓝色文字主颜色
	 * @value color-blue-help 蓝色文字辅颜色
	 * @value color-purple 紫色文字主颜色
	 * @value color-purple-help 紫色文字辅颜色
	 * @value color-black 黑色文字主颜色
	 * @value color-black-help 黑色文字辅颜色
	 * @value color-gray 灰色文字主颜色
	 * @value color-gray-help 灰色文字辅颜色
	 * @value color-white 白色文字主颜色
	 * @value color-white-help 白色文字辅颜色
	 * * @property {String} inactive-color = [color-main|color-often|color-help|color-fill|color-theme|color-theme-help|color-red|color-red-help|color-orange|color-orange-help|color-yellow|color-yellow-help|color-green|color-green-help|color-cyan|color-cyan-help|color-blue|color-blue-help|color-purple|color-purple-help|color-black|color-black-help|color-gray|color-gray-help|color-white|color-white-help] 未激活文本颜色
	 * @value color-main 主要文字颜色
	 * @value color-often 常规文字颜色
	 * @value color-help 次要/辅助文字颜色
	 * @value color-fill 占位文字颜色
	 * @value color-theme 主题文字主颜色
	 * @value color-theme-help 主题文字辅颜色
	 * @value color-red 红色文字主颜色
	 * @value color-red-help 红色文字辅颜色
	 * @value color-orange 橙色文字主颜色
	 * @value color-orange-help 橙色文字辅颜色
	 * @value color-yellow 黄色文字主颜色
	 * @value color-yellow-help 黄色文字辅颜色
	 * @value color-green 绿色文字主颜色
	 * @value color-green-help 绿色文字辅颜色
	 * @value color-cyan 青色文字主颜色
	 * @value color-cyan-help 青色文字辅颜色
	 * @value color-blue 蓝色文字主颜色
	 * @value color-blue-help 蓝色文字辅颜色
	 * @value color-purple 紫色文字主颜色
	 * @value color-purple-help 紫色文字辅颜色
	 * @value color-black 黑色文字主颜色
	 * @value color-black-help 黑色文字辅颜色
	 * @value color-gray 灰色文字主颜色
	 * @value color-gray-help 灰色文字辅颜色
	 * @value color-white 白色文字主颜色
	 * @value color-white-help 白色文字辅颜色
	 */
	export default {
		name: "unifySelect",
		props: {
			unifyData: {
				type: Array,
				default: function() { //数据结构为：[{name:A,value:B,checked:true},{name:C,value:D,checked:false}]
					return []
				}
			},
			mode: {
				type: String, //radio单选/check复选
				default: "radio"
			},
			size: {
				type: [String, Number], //字号
				default: 28
			},
			height: {
				type: [String, Number], //高度
				default: 60
			},
			width: {
				type: [String, Number], //宽度，默认为空值，空值状态弹性布局
				default: ""
			},
			activeBg: {
				type: String, //激活背景色
				default: "bg-theme-help"
			},
			inactiveBg: {
				type: String, //未激活背景色
				default: "bg-white-help"
			},
			activeColor: {
				type: String, //激活文本颜色
				default: "color-main"
			},
			inactiveColor: {
				type: String, //未激活文本颜色
				default: "color-main"
			}
		},
		data() {
			return {
				selectData: []
			};
		},
		watch: {
			mode: function() {
				this.selectData = JSON.parse(JSON.stringify(this.unifyData));
			}
		},
		created() {
			this.selectData = JSON.parse(JSON.stringify(this.unifyData));
		},
		methods: {
			select: function(index) {
				if (this.mode == 'radio') {
					for (let i in this.selectData) {
						if (i == index) {
							this.selectData[i].checked = true;
							this.$emit("selected", this.selectData[i].value); //单选返回值
						} else {
							this.selectData[i].checked = false;
						}
					}
				} else if (this.mode == 'check') {
					this.selectData[index].checked = !this.selectData[index].checked;
					let selectList = [];
					for (let i in this.selectData) {
						if (this.selectData[i].checked) {
							selectList.push(this.selectData[i].value)
						}
					};
					console.log(selectList)
					this.$emit("selected", selectList); //多选返回值数组
				}
			}
		}
	}
</script>

<style lang="scss">

</style>
