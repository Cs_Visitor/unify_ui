<template>
	<view :class="[bg,'row-middle']" :style="{height: height+'rpx'}">
		<view class="row-nowrap padding-lr-md">
			<unify-icon :size="size" :icon="iconName" :color="iconColor"></unify-icon>
		</view>
		<text class="text-bold ellipsis-1 flex-1" :class="[color]" :style="{fontSize:size+'rpx'}">{{text}}</text>
	</view>
</template>

<script>
	/**
	 * title 标题
	 * @description 标题组件
	 * @tutorial 
	 * @property {String} text 标题文本
	 * @property {String, Number} size = [20|24|28|32|48|56] 标题文号
	 * @property {String} color = [color-main|color-often|color-help|color-fill|color-theme|color-theme-help|color-red|color-red-help|color-orange|color-orange-help|color-yellow|color-yellow-help|color-green|color-green-help|color-cyan|color-cyan-help|color-blue|color-blue-help|color-purple|color-purple-help|color-black|color-black-help|color-gray|color-gray-help|color-white|color-white-help] 文字颜色
	 * @value color-main 主要文字颜色
	 * @value color-often 常规文字颜色
	 * @value color-help 次要/辅助文字颜色
	 * @value color-fill 占位文字颜色
	 * @value color-theme 主题文字主颜色
	 * @value color-theme-help 主题文字辅颜色
	 * @value color-red 红色文字主颜色
	 * @value color-red-help 红色文字辅颜色
	 * @value color-orange 橙色文字主颜色
	 * @value color-orange-help 橙色文字辅颜色
	 * @value color-yellow 黄色文字主颜色
	 * @value color-yellow-help 黄色文字辅颜色
	 * @value color-green 绿色文字主颜色
	 * @value color-green-help 绿色文字辅颜色
	 * @value color-cyan 青色文字主颜色
	 * @value color-cyan-help 青色文字辅颜色
	 * @value color-blue 蓝色文字主颜色
	 * @value color-blue-help 蓝色文字辅颜色
	 * @value color-purple 紫色文字主颜色
	 * @value color-purple-help 紫色文字辅颜色
	 * @value color-black 黑色文字主颜色
	 * @value color-black-help 黑色文字辅颜色
	 * @value color-gray 灰色文字主颜色
	 * @value color-gray-help 灰色文字辅颜色
	 * @value color-white 白色文字主颜色
	 * @value color-white-help 白色文字辅颜色
	 * @property {String} bg = [bg-theme|bg-theme-help|bg-red|bg-red-help|bg-orange|bg-orange-help|bg-yellow|bg-yellow-help|bg-green|bg-green-help|bg-cyan|bg-cyan-help|bg-blue|bg-blue-help|bg-purple|bg-purple-help|bg-black|bg-black-help|bg-gray|bg-gray-help|bg-white|bg-white-help|bg-theme-top|bg-theme-bottom|bg-theme-left|bg-theme-right|bg-red-top|bg-red-bottom|bg-red-left|bg-red-right|bg-orange-top|bg-orange-bottom|bg-orange-left|bg-orange-right|bg-yellow-top|bg-yellow-bottom|bg-yellow-left|bg-yellow-right|bg-green-top|bg-green-bottom|bg-green-left|bg-green-right|bg-cyan-top|bg-cyan-bottom|bg-cyan-left|bg-cyan-right|bg-blue-top|bg-blue-bottom|bg-blue-left|bg-blue-right|bg-purple-top|bg-purple-bottom|bg-purple-left|bg-purple-right|bg-black-top|bg-black-bottom|bg-black-left|bg-black-right|bg-gray-top|bg-gray-bottom|bg-gray-left|bg-gray-right|bg-white-top|bg-white-bottom|bg-white-left|bg-white-right] 标题背景色
	 * @value bg-theme 主题背景主色，可在uni.scss自行配置颜色值
	 * @value bg-theme-help 主题背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-theme-top 主题色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-theme-bottom 主题色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-theme-left 主题色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-theme-right 主题色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-red 红色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-red-help 红色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-red-top 红色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-red-bottom 红色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-red-left 红色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-red-right 红色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-orange 橙色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-orange-help 橙色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-orange-top 橙色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-orange-bottom 橙色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-orange-left 橙色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-orange-right 橙色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-yellow 黄色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-yellow-help 黄色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-yellow-top 黄色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-yellow-bottom 黄色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-yellow-left 黄色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-yellow-right 黄色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-green 绿色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-green-help 绿色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-green-top 绿色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-green-bottom 绿色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-green-left 绿色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-green-right 绿色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-cyan 青色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-cyan-help 青色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-cyan-top 青色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-cyan-bottom 青色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-cyan-left 青色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-cyan-right 青色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-blue 蓝色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-blue-help 蓝色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-blue-top 蓝色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-blue-bottom 蓝色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-blue-left 蓝色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-blue-right 蓝色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-purple 紫色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-purple-help 紫色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-purple-top 紫色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-purple-bottom 紫色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-purple-left 紫色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-purple-right 紫色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-black 黑色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-black-help 黑色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-black-top 黑色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-black-bottom 黑色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-black-left 黑色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-black-right 黑色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-gray 灰色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-gray-help 灰色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-gray-top 灰色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-gray-bottom 灰色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-gray-left 灰色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-gray-right 灰色渐变（左主右辅），由主色与辅色渐变而成
	 * @value bg-white 白色背景主色，可在uni.scss自行配置颜色值
	 * @value bg-white-help 白色背景辅色，可在uni.scss自行配置颜色值
	 * @value bg-white-top 白色渐变（上辅下主），由主色与辅色渐变而成
	 * @value bg-white-bottom 白色渐变（上主下辅），由主色与辅色渐变而成
	 * @value bg-white-left 白色渐变（左辅右主），由主色与辅色渐变而成
	 * @value bg-white-right 白色渐变（左主右辅），由主色与辅色渐变而成
	 * @property {String, Number} height = [88|96|112] 标题高度
	 * @property {String} iconName = [] 图标名称
	 * @property {String} icon-color = [color-main|color-often|color-help|color-fill|color-theme|color-theme-help|color-red|color-red-help|color-orange|color-orange-help|color-yellow|color-yellow-help|color-green|color-green-help|color-cyan|color-cyan-help|color-blue|color-blue-help|color-purple|color-purple-help|color-black|color-black-help|color-gray|color-gray-help|color-white|color-white-help] 图标颜色
	 * @value color-main 主要文字颜色
	 * @value color-often 常规文字颜色
	 * @value color-help 次要/辅助文字颜色
	 * @value color-fill 占位文字颜色
	 * @value color-theme 主题文字主颜色
	 * @value color-theme-help 主题文字辅颜色
	 * @value color-red 红色文字主颜色
	 * @value color-red-help 红色文字辅颜色
	 * @value color-orange 橙色文字主颜色
	 * @value color-orange-help 橙色文字辅颜色
	 * @value color-yellow 黄色文字主颜色
	 * @value color-yellow-help 黄色文字辅颜色
	 * @value color-green 绿色文字主颜色
	 * @value color-green-help 绿色文字辅颜色
	 * @value color-cyan 青色文字主颜色
	 * @value color-cyan-help 青色文字辅颜色
	 * @value color-blue 蓝色文字主颜色
	 * @value color-blue-help 蓝色文字辅颜色
	 * @value color-purple 紫色文字主颜色
	 * @value color-purple-help 紫色文字辅颜色
	 * @value color-black 黑色文字主颜色
	 * @value color-black-help 黑色文字辅颜色
	 * @value color-gray 灰色文字主颜色
	 * @value color-gray-help 灰色文字辅颜色
	 * @value color-white 白色文字主颜色
	 * @value color-white-help 白色文字辅颜色
	 */
	export default {
		name: "unifyTitle",
		props: {
			text: {
				type: String,
				default: '标题' //标题文本
			},
			size: {
				type: [String, Number],
				default: 30 //标题尺寸
			},
			color: {
				type: String, //标题颜色
				default: "color-main"
			},
			bg: {
				type: String, //标题背景色
				default: ''
			},
			height: {
				type: [String, Number], //标题高度
				default: 88
			},
			iconName: {
				type: String,
				default: "iconevaluatefill" //图标名称
			},
			iconColor: {
				type: String,
				default: "color-theme" //图标颜色(主题色)
			}
		},
		data() {
			return {

			};
		},
		created() {

		},
		methods: {

		}
	}
</script>

<style>

</style>
