<template>
	<view>
		<image @tap="change" :fade-show="fadeShow" :mode="mode" @error="error" :src="imageSrc" :style="{width:width+'rpx',height:height+'rpx','border-radius':radius+'rpx'}"></image>
	</view>
</template>

<script>
	/**
	 * image 图片
	 * @description 图片组件
	 * @tutorial 
	 * @property {String} src 图片地址
	 * @property {String, Number,Arrar} size 图片尺寸
	 * @property {String, Number} radius = [] 圆角
	 * @property {String} mode = [] 图片裁剪、缩放的模式
	 * @property {String} error-src 加载失败图片
	 * @property {Boolean} fade-show = [false|true] 图片显示动画效果(仅App-nvue 2.3.4+ Android有效)
	 * @event {Function()} click 点击图片触发事件 
	 */
	export default {
		name: "unifyImage",
		props: {
			src: {
				type: String, //图片地址
				default: ""
			},
			size: {
				type: [String, Number, Array], //图片尺寸
				default: 96
			},
			radius: {
				type: [String, Number], //圆角
				default: 0
			},
			mode: {
				type: String,
				default: "aspectFill" //图片裁剪、缩放的模式
			},
			errorSrc: {
				type: String,
				default: "" //加载失败图片
			},
			preview: {
				type: Boolean,
				default: false //是否预览图片
			},
			fadeShow: {
				type: Boolean,
				default: true //图片显示动画效果(仅App-nvue 2.3.4+ Android有效)
			}
		},
		data() {
			return {
				width: 0, //图片宽度
				height: 0, //图片高度
				imageSrc: "" //图片地址
			}
		},
		watch: {
			/* 监听size变化取值，当size为数组时，例如[100,150],数组下标0为宽，下标1为高。当size不是数组时，宽高相等。 */
			size: function() {
				if (Array.isArray(this.size)) {
					this.width = this.size[0];
					this.height = this.size[1];
				} else {
					this.width = this.size;
					this.height = this.size;
				}
			},
			/* 监听src图片 */
			src: function(e) {
				this.imageSrc = this.src
			}
		},
		created() {
			/* 当size为数组时，例如[100,150],数组下标0为宽，下标1为高。当size不是数组时，宽高相等。 */
			if (Array.isArray(this.size)) {
				if (this.size.length == 1) {
					this.width = this.size[0];
					this.height = this.size[0];
				} else if (this.size.length == 2) {
					this.width = this.size[0];
					this.height = this.size[1];
				} else {
					console.error("size参数不正确,当参数为数组时为[1]或者[1,2]")
				}
			} else {
				this.width = this.size;
				this.height = this.size;
			};
			/* 赋值图片地址 */
			this.imageSrc = this.src;
		},
		methods: {
			/* 当图片加载失败时,显示加载失败图片 */
			error: function(e) {
				//当为.gif图时，会直接进入图片加载失败事件，这里判断是否为.gif图，是则不进入渲染加载失败图片
				let src_type = this.src.indexOf(".gif") != -1;
				if (this.src != "" && !src_type) {
					this.imageSrc = this.errorSrc;
				}
			},
			/* 点击事件 */
			change: function() {
				this.$emit("click")
			}
		}
	}
</script>

<style lang="scss">

</style>
