# unify_ui

#### 介绍
基于uni-app平台开发的一套轻量级全端UI框架，适用于nvue与vue页面，核心样式文件遵循weex规范编写，在uni.scss文件可配置全局的样式。编写nvue或者vue文件使用unifyui可以达到APP(nvue/vue)/H5/微信小程序/百度小程序/支付宝小程序/字节跳动小程序/QQ小程序效果达到一致性，unifyui内的组件都全端兼容，unifyui收集了350+常用图标(全端兼容)。unifyui会长期高效率的更新，会侧重于UI模板。

#### 版权信息

unify_ui遵循MIT开源协议，您无需支付任何费用，也无需授权，即可将unify_ui应用到您的项目中。

#### 安装教程

前往[unify_ui文档](https://unify.dengqichang.cn/)